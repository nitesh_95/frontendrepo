package com.subk.highmark.cirifresponse.core;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "HEADER")
public class Header_Data {

	private String date_of_request;
	private String prepared_for;
	private String issued_for;
	private String report_id;
	private String batch_id;
	private String status;
	private String date_of_issue;

	@XmlElement(name = "DATE-OF-ISSUE")
	public String getDate_of_issue() {
		return date_of_issue;
	}

	public void setDate_of_issue(String date_of_issue) {
		this.date_of_issue = date_of_issue;
	}

	@XmlElement(name = "DATE-OF-REQUEST")
	public String getDate_of_request() {
		return date_of_request;
	}

	public void setDate_of_request(String date_of_request) {
		this.date_of_request = date_of_request;
	}

	@XmlElement(name = "PREPARED-FOR")
	public String getPrepared_for() {
		return prepared_for;
	}

	public void setPrepared_for(String prepared_for) {
		this.prepared_for = prepared_for;
	}

	@XmlElement(name = "DATE-OF-ISSUE")
	public String getIssued_for() {
		return issued_for;
	}

	public void setIssued_for(String issued_for) {
		this.issued_for = issued_for;
	}

	@XmlElement(name = "REPORT-ID")
	public String getReport_id() {
		return report_id;
	}

	public void setReport_id(String report_id) {
		this.report_id = report_id;
	}

	@XmlElement(name = "BATCH-ID")
	public String getBatch_id() {
		return batch_id;
	}

	public void setBatch_id(String batch_id) {
		this.batch_id = batch_id;
	}

	@XmlElement(name = "STATUS")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Header_Data [date_of_request=" + date_of_request + ", prepared_for=" + prepared_for + ", issued_for="
				+ issued_for + ", report_id=" + report_id + ", batch_id=" + batch_id + ", status=" + status + "]";
	}

}
