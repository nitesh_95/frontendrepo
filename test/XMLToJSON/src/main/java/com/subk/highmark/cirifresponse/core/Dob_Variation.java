package com.subk.highmark.cirifresponse.core;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "DATE-OF-BIRTH-VARIATIONS")
public class Dob_Variation {

	private List<Variation> variations;

	@XmlElement(name = "VARIATION")
	public List<Variation> getVariations() {
		return variations;
	}

	public void setVariations(List<Variation> variations) {
		this.variations = variations;
	}
	
	
}
