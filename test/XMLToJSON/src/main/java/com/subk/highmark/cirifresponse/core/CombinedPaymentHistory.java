package com.subk.highmark.cirifresponse.core;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.subk.services.PdfGeneration;

public class CombinedPaymentHistory {

	public PdfPTable combinedPaymentHistory(String rat_values) throws Exception {
		PdfGeneration pdf = new PdfGeneration();
		Font data = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.NORMAL, new BaseColor(15, 63, 107));
		PdfPTable table10 = new PdfPTable(13);
		table10.setWidthPercentage(100f);
		table10.setWidths(new float[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });
		table10.addCell(pdf.getCellColSpan("Payment History/Asset Classification:", PdfPCell.ALIGN_MIDDLE, data, 13));
		table10.addCell(pdf.getCell2("Year #:", PdfPCell.ALIGN_MIDDLE, data));
		table10.addCell(pdf.getCell2("Jan", PdfPCell.ALIGN_MIDDLE, data));
		table10.addCell(pdf.getCell2("Feb", PdfPCell.ALIGN_MIDDLE, data));
		table10.addCell(pdf.getCell2("Mar", PdfPCell.ALIGN_MIDDLE, data));
		table10.addCell(pdf.getCell2("Apr", PdfPCell.ALIGN_MIDDLE, data));
		table10.addCell(pdf.getCell2("May", PdfPCell.ALIGN_MIDDLE, data));
		table10.addCell(pdf.getCell2("Jun", PdfPCell.ALIGN_MIDDLE, data));
		table10.addCell(pdf.getCell2("Jul", PdfPCell.ALIGN_MIDDLE, data));
		table10.addCell(pdf.getCell2("Aug", PdfPCell.ALIGN_MIDDLE, data));
		table10.addCell(pdf.getCell2("Sep", PdfPCell.ALIGN_MIDDLE, data));
		table10.addCell(pdf.getCell2("Oct", PdfPCell.ALIGN_MIDDLE, data));
		table10.addCell(pdf.getCell2("Nov", PdfPCell.ALIGN_MIDDLE, data));
		table10.addCell(pdf.getCell2("Dec", PdfPCell.ALIGN_MIDDLE, data));
		table10.setSpacingAfter(3f);
		String tabletr = "<tr><th>Year</th><th>Jan</th><th>Feb</th><th>Mar</th><th>Apr</th><th>May</th><th>Jun</th><th>Jul</th><th>Aug</th><th>Sep</th><th>Oct</th><th>Nov</th><th>Dec</th></tr><tr>";
		String[] ab = rat_values.split("\\|");
		String fy_year = "0";
		for (int i = ab.length - 1; i >= 0; i--) {
			String payment_history = ab[i];
			String[] z = payment_history.split("\\,");
			if (payment_history.length() != 0) {

				if (z.length > 0) {
					String[] y = z[0].split("\\:");
					String month = y[0];
					System.out.println("fy year: " + month + ":" + fy_year + "->" + z[1]);
					if (!y[1].equals(fy_year)) {
						fy_year = y[1];
						if (fy_year != "0") {
							tabletr += "</tr>";
							table10.addCell(pdf.getCell2(y[1], PdfPCell.ALIGN_MIDDLE, data));
						}
						if (y[0].equalsIgnoreCase("Jan")) {
							table10.addCell(pdf.getCell2(z[1], PdfPCell.ALIGN_MIDDLE, data));
						} else if (y[0].equalsIgnoreCase("Feb")) {
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2(z[1], PdfPCell.ALIGN_MIDDLE, data));
						} else if (y[0].equalsIgnoreCase("Mar")) {
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2(z[1], PdfPCell.ALIGN_MIDDLE, data));
						} else if (y[0].equalsIgnoreCase("Apr")) {

							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2(z[1], PdfPCell.ALIGN_MIDDLE, data));
						} else if (y[0].equalsIgnoreCase("May")) {

							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2(z[1], PdfPCell.ALIGN_MIDDLE, data));
						} else if (y[0].equalsIgnoreCase("Jun")) {

							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2(z[1], PdfPCell.ALIGN_MIDDLE, data));
						} else if (y[0].equalsIgnoreCase("Jul")) {

							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2(z[1], PdfPCell.ALIGN_MIDDLE, data));
						} else if (y[0].equalsIgnoreCase("Aug")) {

							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2(z[1], PdfPCell.ALIGN_MIDDLE, data));
						} else if (y[0].equalsIgnoreCase("Sep")) {

							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2(z[1], PdfPCell.ALIGN_MIDDLE, data));
						} else if (y[0].equalsIgnoreCase("Oct")) {

							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2(z[1], PdfPCell.ALIGN_MIDDLE, data));
						} else if (y[0].equalsIgnoreCase("Nov")) {

							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2(z[1], PdfPCell.ALIGN_MIDDLE, data));
						} else if (y[0].equalsIgnoreCase("Dec")) {

							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
							table10.addCell(pdf.getCell2(z[1], PdfPCell.ALIGN_MIDDLE, data));
						}
					} else {
						System.out.println("else*********************");
						if (y[0].equalsIgnoreCase("Jan")) {
							table10.addCell(pdf.getCell2(z[1], PdfPCell.ALIGN_MIDDLE, data));
						} else if (y[0].equalsIgnoreCase("Feb")) {
							table10.addCell(pdf.getCell2(z[1], PdfPCell.ALIGN_MIDDLE, data));
						} else if (y[0].equalsIgnoreCase("Mar")) {
							table10.addCell(pdf.getCell2(z[1], PdfPCell.ALIGN_MIDDLE, data));
						} else if (y[0].equalsIgnoreCase("Apr")) {
							table10.addCell(pdf.getCell2(z[1], PdfPCell.ALIGN_MIDDLE, data));
						} else if (y[0].equalsIgnoreCase("May")) {
							table10.addCell(pdf.getCell2(z[1], PdfPCell.ALIGN_MIDDLE, data));
						} else if (y[0].equalsIgnoreCase("Jun")) {
							table10.addCell(pdf.getCell2(z[1], PdfPCell.ALIGN_MIDDLE, data));
						} else if (y[0].equalsIgnoreCase("Jul")) {
							table10.addCell(pdf.getCell2(z[1], PdfPCell.ALIGN_MIDDLE, data));
						} else if (y[0].equalsIgnoreCase("Aug")) {
							table10.addCell(pdf.getCell2(z[1], PdfPCell.ALIGN_MIDDLE, data));
						} else if (y[0].equalsIgnoreCase("Sep")) {
							table10.addCell(pdf.getCell2(z[1], PdfPCell.ALIGN_MIDDLE, data));
						} else if (y[0].equalsIgnoreCase("Oct")) {
							table10.addCell(pdf.getCell2(z[1], PdfPCell.ALIGN_MIDDLE, data));
						} else if (y[0].equalsIgnoreCase("Nov")) {
							table10.addCell(pdf.getCell2(z[1], PdfPCell.ALIGN_MIDDLE, data));
						} else if (y[0].equalsIgnoreCase("Dec")) {
							table10.addCell(pdf.getCell2(z[1], PdfPCell.ALIGN_MIDDLE, data));
						} else {
							table10.addCell(pdf.getCell2("-", PdfPCell.ALIGN_MIDDLE, data));
						}
					}
				}
			}
		}
		System.out.println("table roe:: " + tabletr);
		return table10;
	}
}