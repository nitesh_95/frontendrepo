/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.highmark.cirifresponse.core;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abhilashd
 */
@XmlRootElement(name="LOAN-DETAILS")
public class Loan_Details {
    private String acct_number;
    private String credit_guarantor;
    private String acct_type;
    private String date_reported;
    private String ownership_ind;
    private String account_status;
    private String disbursed_amt;
    private String disbursed_dt;
    private String last_payment_date;
    private String closed_date;
    private String overdue_amt;
    private String write_off_amt;
    private String current_bal;
    private String combined_payment_history;
    private String matched_type;
    private LinkedAccounts linked_accounts;
    private SecurityDetails security_details;
    private String written_off_settled_status;
    private String installment_amount;
    

   @XmlElement(name="ACCT-NUMBER")
    public String getAcct_number() {
        return acct_number;
    }

    public void setAcct_number(String acct_number) {
        this.acct_number = acct_number;
    }

    @XmlElement(name="CREDIT-GUARANTOR")
    public String getCredit_guarantor() {
        return credit_guarantor;
    }

    public void setCredit_guarantor(String credit_guarantor) {
        this.credit_guarantor = credit_guarantor;
    }
    @XmlElement(name="ACCT-TYPE")
    public String getAcct_type() {
        return acct_type;
    }

    public void setAcct_type(String acct_type) {
        this.acct_type = acct_type;
    }
    @XmlElement(name="DATE-REPORTED")
    public String getDate_reported() {
        return date_reported;
    }

    public void setDate_reported(String date_reported) {
        this.date_reported = date_reported;
    }
    @XmlElement(name="OWNERSHIP-IND")
    public String getOwnership_ind() {
        return ownership_ind;
    }

    public void setOwnership_ind(String ownership_ind) {
        this.ownership_ind = ownership_ind;
    }
    @XmlElement(name="ACCOUNT-STATUS")
    public String getAccount_status() {
        return account_status;
    }

    public void setAccount_status(String account_status) {
        this.account_status = account_status;
    }
    @XmlElement(name="DISBURSED-AMT")
    public String getDisbursed_amt() {
        return disbursed_amt;
    }

    public void setDisbursed_amt(String disbursed_amt) {
        this.disbursed_amt = disbursed_amt;
    }
    @XmlElement(name="DISBURSED-DT")
    public String getDisbursed_dt() {
        return disbursed_dt;
    }

    public void setDisbursed_dt(String disbursed_dt) {
        this.disbursed_dt = disbursed_dt;
    }
    @XmlElement(name="LAST-PAYMENT-DATE")
    public String getLast_payment_date() {
        return last_payment_date;
    }

    public void setLast_payment_date(String last_payment_date) {
        this.last_payment_date = last_payment_date;
    }
    @XmlElement(name="CLOSED-DATE")
    public String getClosed_date() {
        return closed_date;
    }

    public void setClosed_date(String closed_date) {
        this.closed_date = closed_date;
    }
    @XmlElement(name="OVERDUE-AMT")
    public String getOverdue_amt() {
        return overdue_amt;
    }

    public void setOverdue_amt(String overdue_amt) {
        this.overdue_amt = overdue_amt;
    }
    @XmlElement(name="WRITE-OFF-AMT")
    public String getWrite_off_amt() {
        return write_off_amt;
    }

    public void setWrite_off_amt(String write_off_amt) {
        this.write_off_amt = write_off_amt;
    }
    @XmlElement(name="CURRENT-BAL")
    public String getCurrent_bal() {
        return current_bal;
    }

    public void setCurrent_bal(String current_bal) {
        this.current_bal = current_bal;
    }
    @XmlElement(name="COMBINED-PAYMENT-HISTORY")
    public String getCombined_payment_history() {
        return combined_payment_history;
    }

    public void setCombined_payment_history(String combined_payment_history) {
        this.combined_payment_history = combined_payment_history;
    }
    @XmlElement(name="MATCHED-TYPE")
    public String getMatched_type() {
        return matched_type;
    }

    public void setMatched_type(String matched_type) {
        this.matched_type = matched_type;
    }
    
    @XmlElement(name="LINKED-ACCOUNTS")
    public LinkedAccounts getLinked_accounts() {
        return linked_accounts;
    }

    public void setLinked_accounts(LinkedAccounts linked_accounts) {
        this.linked_accounts = linked_accounts;
    }

    @XmlElement(name="SECURITY-DETAILS")
    public SecurityDetails getSecurity_details() {
        return security_details;
    }

    public void setSecurity_details(SecurityDetails security_details) {
        this.security_details = security_details;
    }

    @XmlElement(name="WRITTEN-OFF_SETTLED-STATUS")
    public String getWritten_off_settled_status() {
        return written_off_settled_status;
    }

    public void setWritten_off_settled_status(String written_off_settled_status) {
        this.written_off_settled_status = written_off_settled_status;
    }

	@XmlElement(name = "INSTALLMENT-AMT")
	public String getInstallment_amount() {
		return installment_amount;
	}

	public void setInstallment_amount(String installment_amount) {
		this.installment_amount = installment_amount;
	}

	@Override
	public String toString() {
		return "Loan_Details [acct_number=" + acct_number + ", credit_guarantor=" + credit_guarantor + ", acct_type="
				+ acct_type + ", date_reported=" + date_reported + ", ownership_ind=" + ownership_ind
				+ ", account_status=" + account_status + ", disbursed_amt=" + disbursed_amt + ", disbursed_dt="
				+ disbursed_dt + ", last_payment_date=" + last_payment_date + ", closed_date=" + closed_date
				+ ", overdue_amt=" + overdue_amt + ", write_off_amt=" + write_off_amt + ", current_bal=" + current_bal
				+ ", combined_payment_history=" + combined_payment_history + ", matched_type=" + matched_type
				+ ", linked_accounts=" + linked_accounts + ", security_details=" + security_details
				+ ", written_off_settled_status=" + written_off_settled_status + ", installment_amount="
				+ installment_amount + "]";
	}
	
            
}
