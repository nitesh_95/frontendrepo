package com.subk.highmark.cirifresponse.core;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="LINKED-ACCOUNTS")
public class LinkedAccounts {

	private AccountDetails accountDetails;

	@XmlElement(name="ACCOUNT-DETAILS")
	public AccountDetails getAccountDetails() {
		return accountDetails;
	}

	public void setAccountDetails(AccountDetails accountDetails) {
		this.accountDetails = accountDetails;
	}
	
}
