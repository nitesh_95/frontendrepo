/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.highmark.cirifresponse.core;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abhilashd
 */
@XmlRootElement(name = "ACCOUNTS-SUMMARY")
public class Accounts_Summary {

    private Derived_Attributes derived_attributes;
    private Primary_Accounts_Summary primary_accounts_summary;
    private Secondary_Accounts_Summary secondary_accounts_summary;

    @XmlElement(name = "DERIVED-ATTRIBUTES")
    public Derived_Attributes getDerived_attributes() {
        return derived_attributes;
    }

    public void setDerived_attributes(Derived_Attributes derived_attributes) {
        this.derived_attributes = derived_attributes;
    }
    @XmlElement(name = "PRIMARY-ACCOUNTS-SUMMARY")
    public Primary_Accounts_Summary getPrimary_accounts_summary() {
        return primary_accounts_summary;
    }

    public void setPrimary_accounts_summary(Primary_Accounts_Summary primary_accounts_summary) {
        this.primary_accounts_summary = primary_accounts_summary;
    }
    @XmlElement(name = "SECONDARY-ACCOUNTS-SUMMARY")
    public Secondary_Accounts_Summary getSecondary_accounts_summary() {
        return secondary_accounts_summary;
    }

    public void setSecondary_accounts_summary(Secondary_Accounts_Summary secondary_accounts_summary) {
        this.secondary_accounts_summary = secondary_accounts_summary;
    }
    
    
}
