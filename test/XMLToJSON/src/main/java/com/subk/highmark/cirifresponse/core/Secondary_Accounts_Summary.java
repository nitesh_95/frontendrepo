/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.highmark.cirifresponse.core;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abhilashd
 */
@XmlRootElement(name = "SECONDARY-ACCOUNTS-SUMMARY")
public class Secondary_Accounts_Summary {

    private String secondaroy_number_of_accounts;
    private String secondaroy_active_number_of_accounts;
    private String secondaroy_overdue_number_of_accounts;
    private String secondaroy_secured_number_of_accouts;
    private String secondaroy_unsecured_number_of_accounts;
    private String secondaroy_untagged_number_of_accounts;
    private String secondaroy_current_balance;
    private String secondaroy_sanctioned_amount;
    private String secondaroy_disbursed_amount;

    @XmlElement(name = "SECONDARY-NUMBER-OF-ACCOUNTS")
    public String getSecondaroy_number_of_accounts() {
        return secondaroy_number_of_accounts;
    }

    public void setSecondaroy_number_of_accounts(String secondaroy_number_of_accounts) {
        this.secondaroy_number_of_accounts = secondaroy_number_of_accounts;
    }

    @XmlElement(name = "SECONDARY-ACTIVE-NUMBER-OF-ACCOUNTS")
    public String getSecondaroy_active_number_of_accounts() {
        return secondaroy_active_number_of_accounts;
    }

    public void setSecondaroy_active_number_of_accounts(String secondaroy_active_number_of_accounts) {
        this.secondaroy_active_number_of_accounts = secondaroy_active_number_of_accounts;
    }

    @XmlElement(name = "SECONDARY-OVERDUE-NUMBER-OF-ACCOUNTS")
    public String getSecondaroy_overdue_number_of_accounts() {
        return secondaroy_overdue_number_of_accounts;
    }

    public void setSecondaroy_overdue_number_of_accounts(String secondaroy_overdue_number_of_accounts) {
        this.secondaroy_overdue_number_of_accounts = secondaroy_overdue_number_of_accounts;
    }

    @XmlElement(name = "SECONDARY-SECURED-NUMBER-OF-ACCOUNTS")
    public String getSecondaroy_secured_number_of_accouts() {
        return secondaroy_secured_number_of_accouts;
    }

    public void setSecondaroy_secured_number_of_accouts(String secondaroy_secured_number_of_accouts) {
        this.secondaroy_secured_number_of_accouts = secondaroy_secured_number_of_accouts;
    }

    @XmlElement(name = "SECONDARY-UNSECURED-NUMBER-OF-ACCOUNTS")
    public String getSecondaroy_unsecured_number_of_accounts() {
        return secondaroy_unsecured_number_of_accounts;
    }

    public void setSecondaroy_unsecured_number_of_accounts(String secondaroy_unsecured_number_of_accounts) {
        this.secondaroy_unsecured_number_of_accounts = secondaroy_unsecured_number_of_accounts;
    }

    @XmlElement(name = "SECONDARY-UNTAGGED-NUMBER-OF-ACCOUNTS")
    public String getSecondaroy_untagged_number_of_accounts() {
        return secondaroy_untagged_number_of_accounts;
    }

    public void setSecondaroy_untagged_number_of_accounts(String secondaroy_untagged_number_of_accounts) {
        this.secondaroy_untagged_number_of_accounts = secondaroy_untagged_number_of_accounts;
    }

    @XmlElement(name = "SECONDARY-CURRENT-BALANCE")
    public String getSecondaroy_current_balance() {
        return secondaroy_current_balance;
    }

    public void setSecondaroy_current_balance(String secondaroy_current_balance) {
        this.secondaroy_current_balance = secondaroy_current_balance;
    }

    @XmlElement(name = "SECONDARY-SANCTIONED-AMOUNT")
    public String getSecondaroy_sanctioned_amount() {
        return secondaroy_sanctioned_amount;
    }

    public void setSecondaroy_sanctioned_amount(String secondaroy_sanctioned_amount) {
        this.secondaroy_sanctioned_amount = secondaroy_sanctioned_amount;
    }

    @XmlElement(name = "SECONDARY-DISBURSED-AMOUNT")
    public String getSecondaroy_disbursed_amount() {
        return secondaroy_disbursed_amount;
    }

    public void setSecondaroy_disbursed_amount(String secondaroy_disbursed_amount) {
        this.secondaroy_disbursed_amount = secondaroy_disbursed_amount;
    }

}
