/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.highmark.cirifresponse.core;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abhilashd
 */
@XmlRootElement(name = "SCORES")
public class Cirif_Cibil_Scores {
    
    
    private Cirif_Score score;
    
    @XmlElement(name = "SCORE")
    public Cirif_Score getScore() {
        return score;
    }

    public void setScore(Cirif_Score score) {
        this.score = score;
    }
    
    
}
