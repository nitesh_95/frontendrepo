/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.highmark.cirifresponse.core;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abhilashd
 */
@XmlRootElement(name = "BASE-PLUS-REPORTS")
public class BaseReports {
 
    private BaseChildReport base_childReport;
    
    @XmlElement(name = "BASE-PLUS-REPORT")
    public BaseChildReport getBase_childReport() {
        return base_childReport;
    }

    public void setBase_childReport(BaseChildReport base_childReport) {
        this.base_childReport = base_childReport;
    }
    
    
}
