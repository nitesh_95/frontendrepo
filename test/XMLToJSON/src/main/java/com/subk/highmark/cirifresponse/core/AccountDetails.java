package com.subk.highmark.cirifresponse.core;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="ACCOUNT-DETAILS")
public class AccountDetails {
	private String acct_number;
	private String credit_guarantor;
	private String  acct_type; 
	private String date_reported; 
	private String ownership_ind;
	private String account_status;
	private String disbursed_amt;
	private String disbursed_dt;
	private String last_payment_date; 
	private String close_date; 
	private String overdue_amt;
	private String current_bal;
	private String security_details;
	
	
	@XmlElement(name="ACCT-NUMBER")
	public String getAcct_number() {
		return acct_number;
	}
	public void setAcct_number(String acct_number) {
		this.acct_number = acct_number;
	}
	
	@XmlElement(name="CREDIT-GUARANTOR")
	public String getCredit_guarantor() {
		return credit_guarantor;
	}
	public void setCredit_guarantor(String credit_guarantor) {
		this.credit_guarantor = credit_guarantor;
	}
	
	@XmlElement(name="ACCT-TYPE")
	public String getAcct_type() {
		return acct_type;
	}
	public void setAcct_type(String acct_type) {
		this.acct_type = acct_type;
	}
	
	@XmlElement(name="DATE-REPORTED")
	public String getDate_reported() {
		return date_reported;
	}
	public void setDate_reported(String date_reported) {
		this.date_reported = date_reported;
	}
	
	@XmlElement(name="OWNERSHIP-IND")
	public String getOwnership_ind() {
		return ownership_ind;
	}
	public void setOwnership_ind(String ownership_ind) {
		this.ownership_ind = ownership_ind;
	}
	
	@XmlElement(name="ACCOUNT-STATUS")
	public String getAccount_status() {
		return account_status;
	}
	public void setAccount_status(String account_status) {
		this.account_status = account_status;
	}
	
	@XmlElement(name="DISBURSED-AMT")
	public String getDisbursed_amt() {
		return disbursed_amt;
	}
	public void setDisbursed_amt(String disbursed_amt) {
		this.disbursed_amt = disbursed_amt;
	}
	
	@XmlElement(name="DISBURSED-DT")
	public String getDisbursed_dt() {
		return disbursed_dt;
	}
	public void setDisbursed_dt(String disbursed_dt) {
		this.disbursed_dt = disbursed_dt;
	}
	
	@XmlElement(name="LAST-PAYMENT-DATE")
	public String getLast_payment_date() {
		return last_payment_date;
	}
	public void setLast_payment_date(String last_payment_date) {
		this.last_payment_date = last_payment_date;
	}
	
	@XmlElement(name="CLOSE-DATE")
	public String getClose_date() {
		return close_date;
	}
	public void setClose_date(String close_date) {
		this.close_date = close_date;
	}
	
	@XmlElement(name="OVERDUE-AMT")
	public String getOverdue_amt() {
		return overdue_amt;
	}
	public void setOverdue_amt(String overdue_amt) {
		this.overdue_amt = overdue_amt;
	}
	
	@XmlElement(name="CURRENT-BAL")
	public String getCurrent_bal() {
		return current_bal;
	}
	public void setCurrent_bal(String current_bal) {
		this.current_bal = current_bal;
	}
	
	@XmlElement(name="SECURITY-DETAILS")
	public String getSecurity_details() {
		return security_details;
	}
	public void setSecurity_details(String security_details) {
		this.security_details = security_details;
	}
	
	

}
