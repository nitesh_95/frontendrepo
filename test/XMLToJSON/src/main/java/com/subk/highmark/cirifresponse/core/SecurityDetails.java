package com.subk.highmark.cirifresponse.core;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="SECURITY-DETAILS")
public class SecurityDetails {
	
	private SecurityDetail securityDetail;

	@XmlElement(name="SECURITY-DETAIL")
	public SecurityDetail getSecurityDetail() {
		return securityDetail;
	}

	public void setSecurityDetail(SecurityDetail securityDetail) {
		this.securityDetail = securityDetail;
	}
	
}
