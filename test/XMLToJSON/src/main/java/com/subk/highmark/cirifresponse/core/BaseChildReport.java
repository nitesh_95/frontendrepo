/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.highmark.cirifresponse.core;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abhilashd
 */
@XmlRootElement(name = "B2C-REPORT")
public class BaseChildReport {

	private Cirif_Request cirif_Request;
	private Accounts_Summary accounts_summary;
	private Responses responses;
	private Cirif_Cibil_Scores cirif_Cibil_Scores;
	private Printable_Report printablereport;
    private Inquiry_History inquiry_history;
    private Header_Data header_Data;
    private Personal_info_variation personal_info_variation;
    
    
    
    
    @XmlElement(name = "PERSONAL-INFO-VARIATION")
    public Personal_info_variation getPersonal_info_variation() {
		return personal_info_variation;
	}

	public void setPersonal_info_variation(Personal_info_variation personal_info_variation) {
		this.personal_info_variation = personal_info_variation;
	}

	@XmlElement(name = "HEADER")
	public Header_Data getHeader_Data() {
		return header_Data;
	}

	public void setHeader_Data(Header_Data header_Data) {
		this.header_Data = header_Data;
	}

	@XmlElement(name = "PRINTABLE-REPORT")
	public Printable_Report getPrintablereport() {
		return printablereport;
	}

	public void setPrintablereport(Printable_Report printablereport) {
		this.printablereport = printablereport;
	}

	@XmlElement(name = "SCORES")
	public Cirif_Cibil_Scores getCirif_Cibil_Scores() {
		return cirif_Cibil_Scores;
	}

	public void setCirif_Cibil_Scores(Cirif_Cibil_Scores cirif_Cibil_Scores) {
		this.cirif_Cibil_Scores = cirif_Cibil_Scores;
	}

	@XmlElement(name = "REQUEST")
	public Cirif_Request getCirif_Request() {
		return cirif_Request;
	}

	public void setCirif_Request(Cirif_Request cirif_Request) {
		this.cirif_Request = cirif_Request;
	}

	@XmlElement(name = "ACCOUNTS-SUMMARY")
	public Accounts_Summary getAccounts_summary() {
		return accounts_summary;
	}

	public void setAccounts_summary(Accounts_Summary accounts_summary) {
		this.accounts_summary = accounts_summary;
	}

	@XmlElement(name = "RESPONSES")
	public Responses getResponses() {
		return responses;
	}

	public void setResponses(Responses responses) {
		this.responses = responses;
	}

	@XmlElement(name = "INQUIRY-HISTORY")
	public Inquiry_History getInquiry_history() {
		return inquiry_history;
	}

	public void setInquiry_history(Inquiry_History inquiry_history) {
		this.inquiry_history = inquiry_history;
	}

}
