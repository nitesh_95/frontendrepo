package com.subk.highmark.cirifresponse.core;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "DRIVING-LICENSE-VARIATIONS")
public class Driving_license_Variation {

	private List<Variation> variations;

	@XmlElement(name = "VARIATION")
	public List<Variation> getVariations() {
		return variations;
	}

	public void setVariations(List<Variation> variations) {
		this.variations = variations;
	}
	
	
}
