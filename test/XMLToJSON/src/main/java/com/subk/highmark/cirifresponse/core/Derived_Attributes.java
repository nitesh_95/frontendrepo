/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.highmark.cirifresponse.core;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abhilashd
 */
@XmlRootElement(name = "DERIVED-ATTRIBUTES")
public class Derived_Attributes {

    private String inquires_in_last_six_months;
    private String length_of_credit_history_year;
    private String length_of_credit_history_month;
    private String average_account_age_year;
    private String average_account_age_month;
    private String new_accounts_in_last_six_months;
    private String new_delinq_account_in_last_six_months;

    @XmlElement(name = "INQURIES-IN-LAST-SIX-MONTHS")
    public String getInquires_in_last_six_months() {
        return inquires_in_last_six_months;
    }

    public void setInquires_in_last_six_months(String inquires_in_last_six_months) {
        this.inquires_in_last_six_months = inquires_in_last_six_months;
    }

    @XmlElement(name = "LENGTH-OF-CREDIT-HISTORY-YEAR")
    public String getLength_of_credit_history_year() {
        return length_of_credit_history_year;
    }

    public void setLength_of_credit_history_year(String length_of_credit_history_year) {
        this.length_of_credit_history_year = length_of_credit_history_year;
    }

    @XmlElement(name = "LENGTH-OF-CREDIT-HISTORY-MONTH")
    public String getLength_of_credit_history_month() {
        return length_of_credit_history_month;
    }

    public void setLength_of_credit_history_month(String length_of_credit_history_month) {
        this.length_of_credit_history_month = length_of_credit_history_month;
    }

    @XmlElement(name = "AVERAGE-ACCOUNT-AGE-YEAR")
    public String getAverage_account_age_year() {
        return average_account_age_year;
    }

    public void setAverage_account_age_year(String average_account_age_year) {
        this.average_account_age_year = average_account_age_year;
    }

    @XmlElement(name = "AVERAGE-ACCOUNT-AGE-MONTH")
    public String getAverage_account_age_month() {
        return average_account_age_month;
    }

    public void setAverage_account_age_month(String average_account_age_month) {
        this.average_account_age_month = average_account_age_month;
    }

    @XmlElement(name = "NEW-ACCOUNTS-IN-LAST-SIX-MONTHS")
    public String getNew_accounts_in_last_six_months() {
        return new_accounts_in_last_six_months;
    }

    public void setNew_accounts_in_last_six_months(String new_accounts_in_last_six_months) {
        this.new_accounts_in_last_six_months = new_accounts_in_last_six_months;
    }

    @XmlElement(name = "NEW-DELINQ-ACCOUNT-IN-LAST-SIX-MONTHS")
    public String getNew_delinq_account_in_last_six_months() {
        return new_delinq_account_in_last_six_months;
    }

    public void setNew_delinq_account_in_last_six_months(String new_delinq_account_in_last_six_months) {
        this.new_delinq_account_in_last_six_months = new_delinq_account_in_last_six_months;
    }

}
