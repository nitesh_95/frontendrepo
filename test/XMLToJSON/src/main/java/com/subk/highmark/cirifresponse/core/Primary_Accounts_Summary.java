/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.highmark.cirifresponse.core;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abhilashd
 */
@XmlRootElement(name = "PRIMARY-ACCOUNTS-SUMMARY")
public class Primary_Accounts_Summary {

    private String primary_number_of_accounts;
    private String primary_active_number_of_accounts;
    private String primary_overdue_number_of_accounts;
    private String primary_secured_number_of_accouts;
    private String primary_unsecured_number_of_accounts;
    private String primary_untagged_number_of_accounts;
    private String primary_current_balance;
    private String primary_sanctioned_amount;
    private String primary_disbursed_amount;

    @XmlElement(name = "PRIMARY-NUMBER-OF-ACCOUNTS")
    public String getPrimary_number_of_accounts() {
        return primary_number_of_accounts;
    }

    public void setPrimary_number_of_accounts(String primary_number_of_accounts) {
        this.primary_number_of_accounts = primary_number_of_accounts;
    }

    @XmlElement(name = "PRIMARY-ACTIVE-NUMBER-OF-ACCOUNTS")
    public String getPrimary_active_number_of_accounts() {
        return primary_active_number_of_accounts;
    }

    public void setPrimary_active_number_of_accounts(String primary_active_number_of_accounts) {
        this.primary_active_number_of_accounts = primary_active_number_of_accounts;
    }

    @XmlElement(name = "PRIMARY-OVERDUE-NUMBER-OF-ACCOUNTS")
    public String getPrimary_overdue_number_of_accounts() {
        return primary_overdue_number_of_accounts;
    }

    public void setPrimary_overdue_number_of_accounts(String primary_overdue_number_of_accounts) {
        this.primary_overdue_number_of_accounts = primary_overdue_number_of_accounts;
    }

    @XmlElement(name = "PRIMARY-SECURED-NUMBER-OF-ACCOUNTS")
    public String getPrimary_secured_number_of_accouts() {
        return primary_secured_number_of_accouts;
    }

    public void setPrimary_secured_number_of_accouts(String primary_secured_number_of_accouts) {
        this.primary_secured_number_of_accouts = primary_secured_number_of_accouts;
    }

    @XmlElement(name = "PRIMARY-UNSECURED-NUMBER-OF-ACCOUNTS")
    public String getPrimary_unsecured_number_of_accounts() {
        return primary_unsecured_number_of_accounts;
    }

    public void setPrimary_unsecured_number_of_accounts(String primary_unsecured_number_of_accounts) {
        this.primary_unsecured_number_of_accounts = primary_unsecured_number_of_accounts;
    }

    @XmlElement(name = "PRIMARY-UNTAGGED-NUMBER-OF-ACCOUNTS")
    public String getPrimary_untagged_number_of_accounts() {
        return primary_untagged_number_of_accounts;
    }

    public void setPrimary_untagged_number_of_accounts(String primary_untagged_number_of_accounts) {
        this.primary_untagged_number_of_accounts = primary_untagged_number_of_accounts;
    }

    @XmlElement(name = "PRIMARY-CURRENT-BALANCE")
    public String getPrimary_current_balance() {
        return primary_current_balance;
    }

    public void setPrimary_current_balance(String primary_current_balance) {
        this.primary_current_balance = primary_current_balance;
    }

    @XmlElement(name = "PRIMARY-SANCTIONED-AMOUNT")
    public String getPrimary_sanctioned_amount() {
        return primary_sanctioned_amount;
    }

    public void setPrimary_sanctioned_amount(String primary_sanctioned_amount) {
        this.primary_sanctioned_amount = primary_sanctioned_amount;
    }

    @XmlElement(name = "PRIMARY-DISBURSED-AMOUNT")
    public String getPrimary_disbursed_amount() {
        return primary_disbursed_amount;
    }

    public void setPrimary_disbursed_amount(String primary_disbursed_amount) {
        this.primary_disbursed_amount = primary_disbursed_amount;
    }

}
