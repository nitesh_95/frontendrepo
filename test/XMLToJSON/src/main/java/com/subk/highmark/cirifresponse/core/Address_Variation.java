package com.subk.highmark.cirifresponse.core;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ADDRESS-VARIATIONS")
public class Address_Variation {
	private List<Variation> variations;

	@XmlElement(name = "VARIATION")
	public List<Variation> getVariations() {
		return variations;
	}

	public void setVariations(List<Variation> variations) {
		this.variations = variations;
	}
	
	
}

