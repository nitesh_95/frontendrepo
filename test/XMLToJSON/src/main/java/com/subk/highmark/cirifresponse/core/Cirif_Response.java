/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.highmark.cirifresponse.core;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abhilashd
 */
@XmlRootElement(name = "BASE-PLUS-REPORT-FILE")
public class Cirif_Response {
    
    private BaseReports basereports;
    
    @XmlElement(name = "BASE-PLUS-REPORTS")
    public BaseReports getBasereports() {
        return basereports;
    }

    public void setBasereports(BaseReports basereports) {
        this.basereports = basereports;
    }
    
    
    
}
