/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.highmark.cirifresponse.core;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abhilashd
 */
@XmlRootElement(name = "SCORE")
public class Cirif_Score {

    private String score_type;
    private String score_value;
    private String score_comments;

    @XmlElement(name = "SCORE-TYPE")
    public String getScore_type() {
        return score_type;
    }

    public void setScore_type(String score_type) {
        this.score_type = score_type;
    }

    @XmlElement(name = "SCORE-VALUE")
    public String getScore_value() {
        return score_value;
    }

    public void setScore_value(String score_value) {
        this.score_value = score_value;
    }

    @XmlElement(name = "SCORE-COMMENTS")
    public String getScore_comments() {
        return score_comments;
    }

    public void setScore_comments(String score_comments) {
        this.score_comments = score_comments;
    }

}
