package com.subk.highmark.cirifresponse.core;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="SECURITY-DETAIL")
public class SecurityDetail {
	private String security_type;
	private String owner_name;
	private String security_value;
	private String date_of_value;
	private String security_charge;
	private String property_address;
	private String automobile_type;
	private String year_of_manufacture;
	private String registration_number;
	private String engine_number;
	private String chassis_number;
	

	@XmlElement(name="SECURITY-TYPE")
	public String getSecurity_type() {
		return security_type;
	}
	public void setSecurity_type(String security_type) {
		this.security_type = security_type;
	}
	@XmlElement(name="OWNER-NAME")
	public String getOwner_name() {
		return owner_name;
	}
	public void setOwner_name(String owner_name) {
		this.owner_name = owner_name;
	}
	@XmlElement(name="SECURITY-VALUE")
	public String getSecurity_value() {
		return security_value;
	}
	public void setSecurity_value(String security_value) {
		this.security_value = security_value;
	}
	@XmlElement(name="DATE-OF-VALUE")
	public String getDate_of_value() {
		return date_of_value;
	}
	public void setDate_of_value(String date_of_value) {
		this.date_of_value = date_of_value;
	}
	@XmlElement(name="SECURITY-CHARGE")
	public String getSecurity_charge() {
		return security_charge;
	}
	public void setSecurity_charge(String security_charge) {
		this.security_charge = security_charge;
	}
	@XmlElement(name="PROPERTY-ADDRESS")
	public String getProperty_address() {
		return property_address;
	}
	public void setProperty_address(String property_address) {
		this.property_address = property_address;
	}
	@XmlElement(name="AUTOMOBILE-TYPE")
	public String getAutomobile_type() {
		return automobile_type;
	}
	public void setAutomobile_type(String automobile_type) {
		this.automobile_type = automobile_type;
	}
	@XmlElement(name="YEAR-OF-MANUFACTURE")
	public String getYear_of_manufacture() {
		return year_of_manufacture;
	}
	public void setYear_of_manufacture(String year_of_manufacture) {
		this.year_of_manufacture = year_of_manufacture;
	}
	@XmlElement(name="REGISTRATION-NUMBER")
	public String getRegistration_number() {
		return registration_number;
	}
	public void setRegistration_number(String registration_number) {
		this.registration_number = registration_number;
	}
	@XmlElement(name="ENGINE-NUMBER")
	public String getEngine_number() {
		return engine_number;
	}
	public void setEngine_number(String engine_number) {
		this.engine_number = engine_number;
	}
	@XmlElement(name="CHASSIS-NUMBER")
	public String getChassis_number() {
		return chassis_number;
	}
	public void setChassis_number(String chassis_number) {
		this.chassis_number = chassis_number;
	}
	
	


}
