/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.highmark.cirifresponse.core;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abhilashd
 */
@XmlRootElement(name = "RESPONSES")
public class Responses {
    
    @XmlElement(name = "RESPONSE")
    private List<Response> response;

    public List<Response> getRespone() {
        return response;
    }

    public void setRespone(List<Response> respone) {
        this.response = respone;
    }

	@Override
	public String toString() {
		return "Responses [response=" + response + "]";
	}
    
    

    
    
    
}
