package com.subk.highmark.cirifresponse.core;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "VARIATION")
public class Variation {

	private String value;
	private String reported_date;

	@XmlElement(name = "VALUE")
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@XmlElement(name = "REPORTED-DATE")
	public String getReported_date() {
		return reported_date;
	}

	public void setReported_date(String reported_date) {
		this.reported_date = reported_date;
	}

	@Override
	public String toString() {
		return "variation [value=" + value + ", reported_date=" + reported_date + "]";
	}

}
