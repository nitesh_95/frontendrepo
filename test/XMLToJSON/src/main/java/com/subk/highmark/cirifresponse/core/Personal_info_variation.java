package com.subk.highmark.cirifresponse.core;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "PERSONAL-INFO-Variation")
public class Personal_info_variation {

	private Name_Variation name_Variation;
	private Address_Variation address_Variation;
	private Driving_license_Variation driving_license_Variation;
	private Dob_Variation dob_Variation;
	private Voter_id_Variation voter_id_Variation;
	private Phone_no_Variation phone_no_Variation;
	private Emailid_Variation emailid_Variation;
	private Ration_card_Variation ration_card_Variation;
	private Pan_no_Variation pan_no_Variation;
	private Passport_Variation passport_Variation;

	@XmlElement(name = "NAME-VARIATIONS")
	public Name_Variation getName_Variation() {
		return name_Variation;
	}

	public void setName_Variation(Name_Variation name_Variation) {
		this.name_Variation = name_Variation;
	}

	@XmlElement(name = "ADDRESS-VARIATIONS")

	public Address_Variation getAddress_Variation() {
		return address_Variation;
	}

	public void setAddress_Variation(Address_Variation address_Variation) {
		this.address_Variation = address_Variation;
	}

	@XmlElement(name = "DRIVING-LICENSE-VARIATIONS")

	public Driving_license_Variation getDriving_license_Variation() {
		return driving_license_Variation;
	}

	public void setDriving_license_Variation(Driving_license_Variation driving_license_Variation) {
		this.driving_license_Variation = driving_license_Variation;
	}

	@XmlElement(name = "DATE-OF-BIRTH-VARIATIONS")
	public Dob_Variation getDob_Variation() {
		return dob_Variation;
	}

	public void setDob_Variation(Dob_Variation dob_Variation) {
		this.dob_Variation = dob_Variation;
	}

	@XmlElement(name = "VOTER-ID-VARIATIONS")

	public Voter_id_Variation getVoter_id_Variation() {
		return voter_id_Variation;
	}

	public void setVoter_id_Variation(Voter_id_Variation voter_id_Variation) {
		this.voter_id_Variation = voter_id_Variation;
	}

	@XmlElement(name = "PHONE-NUMBER-VARIATIONS")

	public Phone_no_Variation getPhone_no_Variation() {
		return phone_no_Variation;
	}

	public void setPhone_no_Variation(Phone_no_Variation phone_no_Variation) {
		this.phone_no_Variation = phone_no_Variation;
	}

	@XmlElement(name = "EMAIL-VARIATIONS")

	public Emailid_Variation getEmailid_Variation() {
		return emailid_Variation;
	}

	public void setEmailid_Variation(Emailid_Variation emailid_Variation) {
		this.emailid_Variation = emailid_Variation;
	}

	@XmlElement(name = "RATION-CARD-VARIATIONS")
	public Ration_card_Variation getRation_card_Variation() {
		return ration_card_Variation;
	}

	public void setRation_card_Variation(Ration_card_Variation ration_card_Variation) {
		this.ration_card_Variation = ration_card_Variation;
	}

	@XmlElement(name = "PAN-VARIATIONS")
	public Pan_no_Variation getPan_no_Variation() {
		return pan_no_Variation;
	}

	public void setPan_no_Variation(Pan_no_Variation pan_no_Variation) {
		this.pan_no_Variation = pan_no_Variation;
	}

	@XmlElement(name = "PASSPORT-VARIATIONS")
	public Passport_Variation getPassport_Variation() {
		return passport_Variation;
	}

	public void setPassport_Variation(Passport_Variation passport_Variation) {
		this.passport_Variation = passport_Variation;
	}
}
