/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.highmark.cirifresponse.core;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abhilashd
 */
@XmlRootElement(name = "RESPONSE")
public class Response {

    private Loan_Details loan_details;

    @XmlElement(name = "LOAN-DETAILS")
    public Loan_Details getLoan_details() {
        return loan_details;
    }

    public void setLoan_details(Loan_Details loan_details) {
        this.loan_details = loan_details;
    }

	@Override
	public String toString() {
		return "Response [loan_details=" + loan_details + "]";
	}
    

}
