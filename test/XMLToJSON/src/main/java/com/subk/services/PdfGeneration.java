package com.subk.services;

import java.io.FileOutputStream;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.subk.highmark.cirifresponse.core.BaseChildReport;
import com.subk.highmark.cirifresponse.core.CombinedPaymentHistory;

public class PdfGeneration {

	@SuppressWarnings("unused")
	public void createPdf(BaseChildReport baseChildReport) {
		Document document = null;

		PdfWriter writer = null;
		String dest = "E:\\doc_cibil_new.pdf";
		try {

			document = new Document(PageSize.A3);
			writer = PdfWriter.getInstance(document, new FileOutputStream(dest, false));
			Font mainHead = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD, new BaseColor(15, 63, 107));
			Font data = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.NORMAL, new BaseColor(15, 63, 107));
			Font subhead = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD, BaseColor.WHITE);
			Font smalldata = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.DARK_GRAY);

			Font smalldatas = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL, BaseColor.DARK_GRAY);
			document.open();

			String forData = null;
			if (baseChildReport.getHeader_Data().getIssued_for() == null) {
				forData = "";
			} else {
				forData = baseChildReport.getHeader_Data().getIssued_for();
			}
			PdfPTable table2 = new PdfPTable(4);

			table2.setWidthPercentage(100f);
			table2.setWidths(new float[] { 3.4f, 0.9f, 0.6f, 1 });

			table2.addCell(getCell("CONSUMER BASE™+ REPORT", PdfPCell.ALIGN_RIGHT, mainHead));

			String imgPath = "https://skcollections.s3.ap-south-1.amazonaws.com/crif-high-mark.jpg";
			Image img = Image.getInstance(imgPath);
			img.scaleAbsolute(150f, 60f);
			document.add(img);
			table2.addCell(getCellSpan(" ", PdfPCell.ALIGN_MIDDLE, mainHead, 5));
			table2.addCell(getCell1("CHM Ref #", PdfPCell.ALIGN_LEFT, data));
			table2.addCell(getCell1(":" + baseChildReport.getHeader_Data().getBatch_id(), PdfPCell.ALIGN_LEFT, data));
			table2.addCell(getCellSpan("For " + forData, PdfPCell.ALIGN_RIGHT, data, 4));
			table2.addCell(getCell1("Prepared For", PdfPCell.ALIGN_LEFT, data));
			System.out.println("************" + baseChildReport.getHeader_Data().getPrepared_for());
			table2.addCell(
					getCell1(": " + baseChildReport.getHeader_Data().getPrepared_for(), PdfPCell.ALIGN_LEFT, data));

			table2.addCell(getCell1("Application ID", PdfPCell.ALIGN_LEFT, data));
			table2.addCell(getCell1(":" + baseChildReport.getHeader_Data().getReport_id(), PdfPCell.ALIGN_LEFT, data));
			table2.addCell(getCell1("Date of Request", PdfPCell.ALIGN_LEFT, data));
			table2.addCell(
					getCell1(":" + baseChildReport.getHeader_Data().getDate_of_request(), PdfPCell.ALIGN_LEFT, data));
			table2.addCell(getCell1("Date of Issue", PdfPCell.ALIGN_LEFT, data));
			table2.addCell(
					getCell1(":" + baseChildReport.getHeader_Data().getDate_of_issue(), PdfPCell.ALIGN_LEFT, data));
			table2.setSpacingAfter(15f);

			document.add(table2);
			String valuesId = "";
			String valuesGender = "";
			if (baseChildReport.getCirif_Request().getPan_no() == null) {

				valuesId = "";
			} else {
				valuesId = baseChildReport.getCirif_Request().getPan_no();
			}
			if (baseChildReport.getCirif_Request().getGender() == null) {

				valuesGender = "";
			} else {
				valuesGender = baseChildReport.getCirif_Request().getPan_no();
			}
			PdfPTable table3 = new PdfPTable(6);
			table3.setWidthPercentage(100f);
			table3.setWidths(new float[] { 1, 2, 1, 2, 1, 2 });
			table3.addCell(getCellColSpan("	   Inquiry Input Information", PdfPCell.ALIGN_LEFT, subhead, 6))
					.setBackgroundColor(new BaseColor(15, 63, 107));
			;
			table3.addCell(getCell("Name", PdfPCell.ALIGN_LEFT, data));
			table3.addCell(
					getCell(": " + baseChildReport.getCirif_Request().getCust_name(), PdfPCell.ALIGN_LEFT, data));
			table3.addCell(getCell("DOB/Age", PdfPCell.ALIGN_LEFT, data));
			table3.addCell(getCell(":" + baseChildReport.getCirif_Request().getDob(), PdfPCell.ALIGN_LEFT, data));
			table3.addCell(getCell("Gender", PdfPCell.ALIGN_LEFT, data));
			table3.addCell(getCell(":" + valuesGender, PdfPCell.ALIGN_LEFT, data));
			table3.addCell(getCell("Father", PdfPCell.ALIGN_LEFT, data));
			table3.addCell(
					getCell(":" + baseChildReport.getCirif_Request().getFatherName(), PdfPCell.ALIGN_LEFT, data));
			table3.addCell(getCell("Spouse", PdfPCell.ALIGN_LEFT, data));
			table3.addCell(getCell(": ", PdfPCell.ALIGN_LEFT, data));
			table3.addCell(getCell("Mother", PdfPCell.ALIGN_LEFT, data));
			table3.addCell(getCell(":", PdfPCell.ALIGN_LEFT, data));
			table3.addCell(getCell("Phone Number", PdfPCell.ALIGN_LEFT, data));
			table3.addCell(getCell(":" + baseChildReport.getCirif_Request().getPhone_1(), PdfPCell.ALIGN_LEFT, data));
			table3.addCell(getCell("ID(s)", PdfPCell.ALIGN_LEFT, data));
			table3.addCell(getCell(":" + valuesId, PdfPCell.ALIGN_LEFT, data));
			table3.addCell(getCell("Email ID(s)", PdfPCell.ALIGN_LEFT, data));
			table3.addCell(getCell(": " + baseChildReport.getCirif_Request().getEmail1(), PdfPCell.ALIGN_LEFT, data));
			table3.addCell(getCell("Entity	Id", PdfPCell.ALIGN_LEFT, data));
			table3.addCell(getCellColSpan(": gfyuaga", PdfPCell.ALIGN_LEFT, data, 5));
			table3.addCell(getCell("Current	Address", PdfPCell.ALIGN_LEFT, data));
			table3.addCell(getCellColSpan(": " + baseChildReport.getCirif_Request().getAddress_1(), PdfPCell.ALIGN_LEFT,
					data, 5));
			table3.addCell(getCell("Other	Address", PdfPCell.ALIGN_LEFT, data));
			table3.addCell(getCellColSpan(": " + baseChildReport.getCirif_Request().getAddress_2(), PdfPCell.ALIGN_LEFT,
					data, 5));
			document.add(Chunk.NEWLINE);

			table3.setSpacingAfter(4f);
			document.add(table3);
			PdfPTable table4 = new PdfPTable(3);
			table4.setWidthPercentage(100f);
			table4.setWidths(new float[] { 1.3f, 1.3f, 2 });

			table4.addCell(getCellColSpan("	   CRIF HM Score (s):", PdfPCell.ALIGN_LEFT, subhead, 6))
					.setBackgroundColor(new BaseColor(15, 63, 107));
			table4.addCell(getCell("   SCORE NAME", PdfPCell.ALIGN_LEFT, data));
			table4.addCell(getCell("   SCORE", PdfPCell.ALIGN_LEFT, data));
			table4.addCell(getCell("   DESCRIPTION", PdfPCell.ALIGN_LEFT, data));
			table4.addCell(getCell(" " + baseChildReport.getCirif_Cibil_Scores().getScore().getScore_type(),
					PdfPCell.ALIGN_LEFT, data)).setBackgroundColor(new BaseColor(167, 203, 227));
			table4.addCell(getCell(" " + baseChildReport.getCirif_Cibil_Scores().getScore().getScore_value(),
					PdfPCell.ALIGN_LEFT, data)).setBackgroundColor(new BaseColor(167, 203, 227));
			table4.addCell(getCell(" " + baseChildReport.getCirif_Cibil_Scores().getScore().getScore_comments(),
					PdfPCell.ALIGN_LEFT, data)).setBackgroundColor(new BaseColor(167, 203, 227));
			document.add(table4);
			Paragraph p1 = new Paragraph(
					"Tip: A-D: Very Low Risk ; E-G: Low Risk ; H-I: Medium Risk ; J-K: High Risk ; L-M: Very High Risk",
					smalldata);
			p1.setAlignment(Element.ALIGN_RIGHT);
			document.add(p1);

			PdfPTable table5 = new PdfPTable(2);
			table5.setWidthPercentage(100f);
			table5.setWidths(new float[] { 1, 1 });
			table5.addCell(getCellColSpan("	   Personal Information - Variations", PdfPCell.ALIGN_LEFT, subhead, 2))
					.setBackgroundColor(new BaseColor(15, 63, 107));
			table5.setSpacingAfter(15f);
			table5.addCell(getCellColSpan("Name Variation", PdfPCell.ALIGN_LEFT, data, 0))
					.setBackgroundColor(new BaseColor(167, 203, 227));
			table5.addCell(getCellColSpan("ReportedDate", PdfPCell.ALIGN_RIGHT, data, 0))
					.setBackgroundColor(new BaseColor(167, 203, 227));
			for (int nameInfo = 0; nameInfo < baseChildReport.getPersonal_info_variation().getName_Variation()
					.getVariations().size(); nameInfo++) {

				table5.addCell(getCell1(baseChildReport.getPersonal_info_variation().getName_Variation().getVariations()
						.get(nameInfo).getValue(), PdfPCell.ALIGN_LEFT, data));
				table5.addCell(getCell1(baseChildReport.getPersonal_info_variation().getName_Variation().getVariations()
						.get(nameInfo).getReported_date(), PdfPCell.ALIGN_RIGHT, data));

			}
			document.add(Chunk.NEWLINE);
			table5.addCell(getCellColSpan("Id Variation", PdfPCell.ALIGN_LEFT, data, 0))
					.setBackgroundColor(new BaseColor(167, 203, 227));
			table5.addCell(getCellColSpan("ReportedDate", PdfPCell.ALIGN_RIGHT, data, 0))
					.setBackgroundColor(new BaseColor(167, 203, 227));
			for (int voterId = 0; voterId < baseChildReport.getPersonal_info_variation().getVoter_id_Variation()
					.getVariations().size(); voterId++) {

				table5.addCell(getCell1(baseChildReport.getPersonal_info_variation().getVoter_id_Variation()
						.getVariations().get(voterId).getValue(), PdfPCell.ALIGN_LEFT, data));
				table5.addCell(getCell1(baseChildReport.getPersonal_info_variation().getVoter_id_Variation()
						.getVariations().get(voterId).getReported_date(), PdfPCell.ALIGN_RIGHT, data));

			}
			document.add(Chunk.NEWLINE);
			table5.addCell(getCellColSpan("DOB Variation", PdfPCell.ALIGN_LEFT, data, 0))
					.setBackgroundColor(new BaseColor(167, 203, 227));
			table5.addCell(getCellColSpan("ReportedDate", PdfPCell.ALIGN_RIGHT, data, 0))
					.setBackgroundColor(new BaseColor(167, 203, 227));
			for (int dob = 0; dob < baseChildReport.getPersonal_info_variation().getDob_Variation().getVariations()
					.size(); dob++) {

				table5.addCell(getCell1(baseChildReport.getPersonal_info_variation().getDob_Variation().getVariations()
						.get(dob).getValue(), PdfPCell.ALIGN_LEFT, data));
				table5.addCell(getCell1(baseChildReport.getPersonal_info_variation().getDob_Variation().getVariations()
						.get(dob).getReported_date(), PdfPCell.ALIGN_RIGHT, data));

			}
			document.add(Chunk.NEWLINE);
			table5.addCell(getCellColSpan("PhoneNo Variation", PdfPCell.ALIGN_LEFT, data, 0))
					.setBackgroundColor(new BaseColor(167, 203, 227));
			table5.addCell(getCellColSpan("ReportedDate", PdfPCell.ALIGN_RIGHT, data, 0))
					.setBackgroundColor(new BaseColor(167, 203, 227));
			for (int phoneno = 0; phoneno < baseChildReport.getPersonal_info_variation().getPhone_no_Variation()
					.getVariations().size(); phoneno++) {

				table5.addCell(getCell1(baseChildReport.getPersonal_info_variation().getPhone_no_Variation()
						.getVariations().get(phoneno).getValue(), PdfPCell.ALIGN_LEFT, data));
				table5.addCell(getCell1(baseChildReport.getPersonal_info_variation().getPhone_no_Variation()
						.getVariations().get(phoneno).getReported_date(), PdfPCell.ALIGN_RIGHT, data));
			}

			document.add(Chunk.NEWLINE);
			table5.addCell(getCellColSpan("AddressVariaton", PdfPCell.ALIGN_LEFT, data, 0))
					.setBackgroundColor(new BaseColor(167, 203, 227));
			table5.addCell(getCellColSpan("ReportedDate", PdfPCell.ALIGN_RIGHT, data, 0))
					.setBackgroundColor(new BaseColor(167, 203, 227));
			for (int addressinfo = 0; addressinfo < baseChildReport.getPersonal_info_variation().getAddress_Variation()
					.getVariations().size(); addressinfo++) {
				table5.addCell(getCell1(baseChildReport.getPersonal_info_variation().getAddress_Variation()
						.getVariations().get(addressinfo).getValue(), PdfPCell.ALIGN_LEFT, data));
				table5.addCell(getCell1(baseChildReport.getPersonal_info_variation().getAddress_Variation()
						.getVariations().get(addressinfo).getReported_date(), PdfPCell.ALIGN_RIGHT, data));

			}

			document.add(table5);

			document.add(Chunk.NEWLINE);
			PdfPTable table6 = new PdfPTable(1);
			table6.setWidthPercentage(100f);
			table6.setWidths(new float[] { 1 });
			table6.addCell(getCellColSpan("	   Account Summary", PdfPCell.ALIGN_LEFT, subhead, 1))
					.setBackgroundColor(new BaseColor(15, 63, 107));
			table6.setSpacingAfter(0.5f);
			document.add(table6);
			Paragraph p3 = new Paragraph(
					"Tip: Current Balance & Disbursed Amount is considered ONLY for ACTIVE accounts.", smalldata);
			p3.setAlignment(Element.ALIGN_RIGHT);
			p3.setSpacingAfter(18f);
			document.add(p3);
			PdfPTable table7 = new PdfPTable(6);
			table7.setWidthPercentage(100f);
			table7.setWidths(new float[] { 1, 1, 1, 1, 1, 1 });

			table7.addCell(getCell1("Type" + " ", PdfPCell.ALIGN_MIDDLE, data));
			table7.addCell(getCell1("Number of Account(s)", PdfPCell.ALIGN_MIDDLE, data));
			table7.addCell(getCell1("Active Account(s)	", PdfPCell.ALIGN_MIDDLE, data));
			table7.addCell(getCell1("Overdue Account(s)", PdfPCell.ALIGN_MIDDLE, data));
			table7.addCell(getCell1("Current Balance", PdfPCell.ALIGN_MIDDLE, data));
			table7.addCell(getCell1("Amt Disbd/ High Credit	", PdfPCell.ALIGN_MIDDLE, data));

			table7.addCell(getCell1("Primary Match ", PdfPCell.ALIGN_MIDDLE, data));
			table7.addCell(getCell1(baseChildReport.getAccounts_summary().getPrimary_accounts_summary()
					.getPrimary_active_number_of_accounts(), PdfPCell.ALIGN_MIDDLE, data));
			table7.addCell(getCell1(baseChildReport.getAccounts_summary().getPrimary_accounts_summary()
					.getPrimary_active_number_of_accounts(), PdfPCell.ALIGN_MIDDLE, data));
			table7.addCell(getCell1(baseChildReport.getAccounts_summary().getPrimary_accounts_summary()
					.getPrimary_overdue_number_of_accounts(), PdfPCell.ALIGN_MIDDLE, data));
			table7.addCell(getCell1(
					baseChildReport.getAccounts_summary().getPrimary_accounts_summary().getPrimary_current_balance(),
					PdfPCell.ALIGN_MIDDLE, data));
			table7.addCell(getCell1(
					baseChildReport.getAccounts_summary().getPrimary_accounts_summary().getPrimary_disbursed_amount(),
					PdfPCell.ALIGN_MIDDLE, data));

			table7.addCell(getCell1("Secondary Match", PdfPCell.ALIGN_MIDDLE, data));
			table7.addCell(getCell1(baseChildReport.getAccounts_summary().getSecondary_accounts_summary()
					.getSecondaroy_active_number_of_accounts(), PdfPCell.ALIGN_MIDDLE, data));
			table7.addCell(getCell1(baseChildReport.getAccounts_summary().getSecondary_accounts_summary()
					.getSecondaroy_active_number_of_accounts(), PdfPCell.ALIGN_MIDDLE, data));
			table7.addCell(getCell1(baseChildReport.getAccounts_summary().getSecondary_accounts_summary()
					.getSecondaroy_overdue_number_of_accounts(), PdfPCell.ALIGN_MIDDLE, data));
			table7.addCell(getCell1(baseChildReport.getAccounts_summary().getSecondary_accounts_summary()
					.getSecondaroy_current_balance(), PdfPCell.ALIGN_MIDDLE, data));
			table7.addCell(getCell1(baseChildReport.getAccounts_summary().getSecondary_accounts_summary()
					.getSecondaroy_disbursed_amount(), PdfPCell.ALIGN_MIDDLE, data));

			table7.addCell(getCell("Inquiries in last 24 Months:", PdfPCell.ALIGN_MIDDLE, data));
			table7.addCell(getCellSpan(
					baseChildReport.getAccounts_summary().getDerived_attributes().getInquires_in_last_six_months(),
					PdfPCell.ALIGN_MIDDLE, smalldata, 1));

			table7.addCell(getCell("New Account(s) in last 6 Months:", PdfPCell.ALIGN_MIDDLE, data));
			table7.addCell(getCellSpan(
					baseChildReport.getAccounts_summary().getDerived_attributes().getNew_accounts_in_last_six_months(),
					PdfPCell.ALIGN_MIDDLE, smalldata, 1));

			table7.addCell(getCell("New Delinquent Account(s) in last 6 Months:", PdfPCell.ALIGN_MIDDLE, data));
			table7.addCell(getCellSpan(baseChildReport.getAccounts_summary().getDerived_attributes()
					.getNew_delinq_account_in_last_six_months(), PdfPCell.ALIGN_MIDDLE, smalldata, 1));

			table7.setSpacingAfter(35f);

			document.add(table7);
			document.add(new Chunk().setNewPage());
			PdfPTable table8 = new PdfPTable(1);
			table8.setWidthPercentage(100f);
			table8.setWidths(new float[] { 1 });
			table8.addCell(getCellColSpan("	   Account Information", PdfPCell.ALIGN_LEFT, subhead, 6))
					.setBackgroundColor(new BaseColor(15, 63, 107));
			table8.setSpacingAfter(10f);
			document.add(table8);

			int ni = baseChildReport.getResponses().getRespone().size();
			System.out.println("count" + ni);

			for (int n = 0; n < ni; n++) {
				PdfPTable table9 = new PdfPTable(8);
				CombinedPaymentHistory hist = new CombinedPaymentHistory();
				String status = baseChildReport.getResponses().getRespone().get(n).getLoan_details()
						.getAccount_status();
				table9.setWidthPercentage(100f);
				table9.setWidths(new float[] { 0.1f, 1, 1, 1, 1, 1, 1, 1 });
				table9.addCell(getCellSpan(
						baseChildReport.getResponses().getRespone().get(n).getLoan_details().getAccount_status(),
						PdfPCell.ALIGN_MIDDLE, subhead, 7)).setBackgroundColor(new BaseColor(15, 62, 100));
				table9.addCell(getCellColSpan(
						"Account Type: " + " "
								+ baseChildReport.getResponses().getRespone().get(n).getLoan_details().getAcct_type(),
						PdfPCell.ALIGN_LEFT, data, 2));
				table9.addCell(getCellColSpan("Credit Grantor: " + " "
						+ baseChildReport.getResponses().getRespone().get(n).getLoan_details().getCredit_guarantor(),
						PdfPCell.ALIGN_LEFT, data, 2));

				table9.addCell(getCellColSpan(
						"Account : " + " "
								+ baseChildReport.getResponses().getRespone().get(n).getLoan_details().getAcct_number(),
						PdfPCell.ALIGN_LEFT, data, 2));

				table9.addCell(getCellColSpan("Ownership :" + " "
						+ baseChildReport.getResponses().getRespone().get(n).getLoan_details().getOwnership_ind(),
						PdfPCell.ALIGN_LEFT, data, 2));

				table9.addCell(getCellColSpan("Disbursed Date: " + " "
						+ baseChildReport.getResponses().getRespone().get(n).getLoan_details().getDisbursed_dt(),
						PdfPCell.ALIGN_LEFT, data, 2));
				table9.addCell(getCellColSpan("Disbd Amt/High Credit:" + " "
						+ baseChildReport.getResponses().getRespone().get(n).getLoan_details().getDisbursed_amt(),
						PdfPCell.ALIGN_LEFT, data, 2));
				table9.addCell(getCellColSpan("Credit Limit:" + " ", PdfPCell.ALIGN_MIDDLE, data, 2));

				table9.addCell(getCellColSpan("Last Payment Date:" + " "
						+ baseChildReport.getResponses().getRespone().get(n).getLoan_details().getLast_payment_date(),
						PdfPCell.ALIGN_LEFT, data, 2));

				table9.addCell(getCellColSpan(
						"Current Balance:" + " "
								+ baseChildReport.getResponses().getRespone().get(n).getLoan_details().getCurrent_bal(),
						PdfPCell.ALIGN_LEFT, data, 2));
				table9.addCell(getCellColSpan("Cash Limit:" + " ", PdfPCell.ALIGN_LEFT, data, 2));

				table9.addCell(getCellColSpan(
						"Closed Date:" + " "
								+ baseChildReport.getResponses().getRespone().get(n).getLoan_details().getClosed_date(),
						PdfPCell.ALIGN_LEFT, data, 2));
				table9.addCell(getCellColSpan("Last Paid Amt:" + " "
						+ baseChildReport.getResponses().getRespone().get(n).getLoan_details().getLast_payment_date(),
						PdfPCell.ALIGN_LEFT, data, 2));

				table9.addCell(getCellColSpan(" InstlAmt/Freq:" + " "
						+ baseChildReport.getResponses().getRespone().get(n).getLoan_details().getInstallment_amount(),
						PdfPCell.ALIGN_LEFT, data, 2));

				table9.addCell(getCellColSpan("Tenure(month):" + " "
						+ baseChildReport.getResponses().getRespone().get(n).getLoan_details().getDate_reported(),
						PdfPCell.ALIGN_LEFT, data, 2));

				table9.addCell(getCellColSpan(
						"Overdue Amt:" + " "
								+ baseChildReport.getResponses().getRespone().get(n).getLoan_details().getOverdue_amt(),
						PdfPCell.ALIGN_LEFT, data, 2));

				table9.setSpacingAfter(3f);
				document.add(Chunk.NEWLINE);
				document.add(Chunk.NEWLINE);
				PdfPTable table10 = hist.combinedPaymentHistory(baseChildReport.getResponses().getRespone().get(n)
						.getLoan_details().getCombined_payment_history());

				document.add(table9);
				document.add(table10);

			}
			document.add(Chunk.NEWLINE);

			PdfPTable table11 = new PdfPTable(6);
			table11.setWidthPercentage(100f);
			table11.setWidths(new float[] { 2, 1, 1, 1, 1, 1 });
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			table11.addCell(getCellColSpan("Inquiries (reported for past 24 months)", PdfPCell.ALIGN_LEFT, subhead, 6))
					.setBackgroundColor(new BaseColor(15, 63, 107));
			Chunk linebreakssh = new Chunk(new LineSeparator());
			document.add(linebreakssh);
			table11.addCell(getCell1("Member Name", PdfPCell.ALIGN_LEFT, data));
			table11.addCell(getCell1("Date of Inquiry", PdfPCell.ALIGN_LEFT, data));
			table11.addCell(getCell1("Purpose", PdfPCell.ALIGN_LEFT, data));
			table11.addCell(getCell1("Ownership Type", PdfPCell.ALIGN_LEFT, data));
			table11.addCell(getCell1("Amount", PdfPCell.ALIGN_LEFT, data));
			table11.addCell(getCell1("Remark", PdfPCell.ALIGN_LEFT, data));
			
			for (int i = 0; i < baseChildReport.getInquiry_history().getHistory().size(); i++) {
				table11.addCell(getCell1(baseChildReport.getInquiry_history().getHistory().get(i).getMember_name(),
						PdfPCell.ALIGN_LEFT, data));
				table11.addCell(getCell1(baseChildReport.getInquiry_history().getHistory().get(i).getInquiry_date(),
						PdfPCell.ALIGN_LEFT, data));
				table11.addCell(getCell1(baseChildReport.getInquiry_history().getHistory().get(i).getPurpose(),
						PdfPCell.ALIGN_LEFT, data));
				table11.addCell(getCell1(baseChildReport.getInquiry_history().getHistory().get(i).getOwnership_type(),
						PdfPCell.ALIGN_LEFT, data));
				table11.addCell(getCell1(baseChildReport.getInquiry_history().getHistory().get(i).getAmount(),
						PdfPCell.ALIGN_LEFT, data));
				table11.addCell(getCell1(baseChildReport.getInquiry_history().getHistory().get(i).getRemark(),
						PdfPCell.ALIGN_LEFT, data));
			}
			table11.setSpacingAfter(20f);
			document.add(table11);
			document.newPage();
			PdfPTable table13 = new PdfPTable(2);
			table13.setWidthPercentage(100f);
			table13.setWidths(new float[] { 1, 1 });

			table13.addCell(getCellColSpan("Comments", PdfPCell.ALIGN_LEFT, subhead, 6))
					.setBackgroundColor(new BaseColor(15, 63, 107));
			Chunk linebreakss = new Chunk(new LineSeparator());
			document.add(linebreakss);
			table13.addCell(getCell1("Description", PdfPCell.ALIGN_LEFT, data));
			table13.addCell(getCell1("Date", PdfPCell.ALIGN_RIGHT, data));
		

			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			table11.addCell(getCell1("-END OF CONSUMER BASE + REPORT-", PdfPCell.ALIGN_MIDDLE, data));

			table13.setSpacingAfter(20f);
			document.add(table13);

			PdfPTable table12 = new PdfPTable(3);
			table12.setWidthPercentage(100f);
			table12.setWidths(new float[] { 2, 1, 2 });

			table12.addCell(getCellColSpan("	   Appendix", PdfPCell.ALIGN_LEFT, subhead, 3))
					.setBackgroundColor(new BaseColor(15, 63, 107));

			Chunk linebreaks = new Chunk(new LineSeparator());
			document.add(linebreaks);
			table12.addCell(getCellColSpan("Section", PdfPCell.ALIGN_LEFT, data, 0));
			table12.addCell(getCellColSpan("Code", PdfPCell.ALIGN_MIDDLE, data, 1));
			table12.addCell(getCellColSpan("Description", PdfPCell.ALIGN_RIGHT, data, 2));

		
			// sectionCode
			table12.addCell(getCell(" Account Summary", PdfPCell.ALIGN_LEFT, data));
			table12.addCell(getCell("	Number of Delinquent Accounts", PdfPCell.ALIGN_MIDDLE, data));
			table12.addCell(getCell(
					" 	Indicates number of accounts that the applicant has defaulted on within the last 6 months",
					PdfPCell.ALIGN_MIDDLE, data));
			table12.addCell(getCell("Account Information - Credit Grantor ", PdfPCell.ALIGN_LEFT, data));
			table12.addCell(getCell(" XXXX", PdfPCell.ALIGN_MIDDLE, data));
			table12.addCell(
					getCell("Name of grantor undisclosed as credit grantor is different from inquiring institution ",
							PdfPCell.ALIGN_MIDDLE, data));
			table12.addCell(getCell("Account Information - Account # ", PdfPCell.ALIGN_LEFT, data));
			table12.addCell(getCell("xxxx", PdfPCell.ALIGN_MIDDLE, data));
			table12.addCell(
					getCell("Account Number undisclosed as credit grantor is different from inquiring institution",
							PdfPCell.ALIGN_RIGHT, data));
			table12.addCell(getCell("Additional Status ", PdfPCell.ALIGN_LEFT, data));
			table12.addCell(getCell("	O/S ", PdfPCell.ALIGN_MIDDLE, data));
			table12.addCell(getCell("Outstanding", PdfPCell.ALIGN_RIGHT, data));
			table12.addCell(getCell("Inquiry Details", PdfPCell.ALIGN_MIDDLE, data));
			table12.addCell(getCell(" DUNS", PdfPCell.ALIGN_MIDDLE, data));
			table12.addCell(getCell("Data Unique Numbering System", PdfPCell.ALIGN_RIGHT, data));
			table12.addCell(getCell(" Inquiry Details", PdfPCell.ALIGN_LEFT, data));
			table12.addCell(getCell("PAN", PdfPCell.ALIGN_MIDDLE, data));
			table12.addCell(getCell("Permanent Account Number", PdfPCell.ALIGN_RIGHT, data));
			table12.addCell(getCell("Inquiry Details ", PdfPCell.ALIGN_LEFT, data));
			table12.addCell(getCell("TAN/TIN ", PdfPCell.ALIGN_MIDDLE, data));
			table12.addCell(getCell("Tax Deduction & Collection Account Number/Taxpayer Identification Number",
					PdfPCell.ALIGN_RIGHT, data));
			table12.addCell(getCell("Inquiry Details ", PdfPCell.ALIGN_LEFT, data));
			table12.addCell(getCell(" CIN/LLPIN", PdfPCell.ALIGN_MIDDLE, data));
			table12.addCell(getCell("ICorporate Identity Number/Limited Liability Partnership Identification Number ",
					PdfPCell.ALIGN_RIGHT, data));
			table12.addCell(getCell(" Relationship Details", PdfPCell.ALIGN_LEFT, data));
			table12.addCell(getCell(" DOB", PdfPCell.ALIGN_MIDDLE, data));
			table12.addCell(getCell(" Date of Birth", PdfPCell.ALIGN_RIGHT, data));
			table12.addCell(getCell(" Payment History / Asset Classification", PdfPCell.ALIGN_LEFT, data));
			table12.addCell(getCell(" 	XXX", PdfPCell.ALIGN_MIDDLE, data));
			table12.addCell(getCell(" Data not reported by institution", PdfPCell.ALIGN_RIGHT, data));
			table12.addCell(getCell(" Payment History / Asset Classification", PdfPCell.ALIGN_LEFT, data));
			table12.addCell(getCell("-", PdfPCell.ALIGN_MIDDLE, data));
			table12.addCell(getCell(" Not applicable", PdfPCell.ALIGN_RIGHT, data));
			table12.addCell(getCell(" Payment History / Asset Classification", PdfPCell.ALIGN_LEFT, data));
			table12.addCell(getCell(" STD", PdfPCell.ALIGN_MIDDLE, data));
			table12.addCell(getCell(" 	Account Reported as STANDARD Asset", PdfPCell.ALIGN_RIGHT, data));
			table12.addCell(getCell(" Payment History / Asset Classification", PdfPCell.ALIGN_LEFT, data));
			table12.addCell(getCell(" SUB", PdfPCell.ALIGN_MIDDLE, data));
			table12.addCell(getCell(" Account Reported as SUB-STANDARD Asset", PdfPCell.ALIGN_RIGHT, data));
			table12.addCell(getCell(" Payment History / Asset Classification", PdfPCell.ALIGN_LEFT, data));
			table12.addCell(getCell(" 	DBT", PdfPCell.ALIGN_MIDDLE, data));
			table12.addCell(getCell(" Account Reported as DOUBTFUL Asset", PdfPCell.ALIGN_RIGHT, data));
			table12.addCell(getCell(" Payment History / Asset Classification", PdfPCell.ALIGN_LEFT, data));
			table12.addCell(getCell(" 	LOS", PdfPCell.ALIGN_MIDDLE, data));
			table12.addCell(getCell(" 	Account Reported as LOSS Asset", PdfPCell.ALIGN_RIGHT, data));
			table12.addCell(getCell(" Payment History / Asset Classification", PdfPCell.ALIGN_LEFT, data));
			table12.addCell(getCell(" SMA", PdfPCell.ALIGN_MIDDLE, data));
			table12.addCell(getCell(" Account Reported as SPECIAL MENTION", PdfPCell.ALIGN_RIGHT, data));
			table12.addCell(getCell(" CRIF HIGH MARK SCORE(S)", PdfPCell.ALIGN_LEFT, data));
			table12.addCell(getCell(" PERFORM-Consumer", PdfPCell.ALIGN_MIDDLE, data));
			table12.addCell(getCell(
					" Score has reckoned from credit history, pursuit of new credit, payment history, type of credit in   use and outstanding debt.",
					PdfPCell.ALIGN_RIGHT, data));

			document.add(table12);

			document.add(Chunk.NEWLINE);
			Chunk linebreak = new Chunk(new LineSeparator());
			document.add(linebreak);

			Paragraph p2 = new Paragraph(
					"Disclaimer:	This document contains proprietary information to CRIF High Mark and may not be used or disclosed to others, except with the written permission of CRIF High Mark. Any paper copy of this document will be considered uncontrolled. If you are not the intended recipient, you are not authorized to read, print, retain, copy, disseminate, distribute, or use this information or any part thereof.PERFORM score provided in this document is joint work of CRIF SPA (Italy) and CRIF High Mark (India).",
					smalldatas);
			p2.setAlignment(Element.ALIGN_LEFT);
			document.add(p2);
			document.add(Chunk.NEWLINE);
			PdfPTable table14 = new PdfPTable(3);
			table14.setWidthPercentage(100f);
			table14.setWidths(new float[] { 1, 1, 1 });
			table14.addCell(getCell(" Copyrights reserved (c) 2019", PdfPCell.ALIGN_LEFT, smalldatas));
			table14.addCell(
					getCell(" CRIF High Mark Credit Information Services Pvt. Ltd", PdfPCell.ALIGN_MIDDLE, smalldatas));
			table14.addCell(getCell(" Company Confidential Data", PdfPCell.ALIGN_RIGHT, smalldatas));

			table14.setSpacingAfter(0.5f);
			document.add(table14);

		} catch (Exception e) {

		}
		document.close();
	}

	public PdfPCell getCell(String string, int alignment, Font canFont) {
		PdfPCell cell = new PdfPCell(new Phrase(string, canFont));
		cell.setPadding(6);
		cell.setHorizontalAlignment(alignment);
		cell.setBorder(PdfPCell.NO_BORDER);
		return cell;
	}

	public PdfPCell getCell1(String string, int alignment, Font canFont) {
		PdfPCell cell = new PdfPCell(new Phrase(string, canFont));
		cell.setPadding(3);
		cell.setHorizontalAlignment(alignment);
		cell.setBorder(PdfPCell.NO_BORDER);
		return cell;
	}

	public PdfPCell getCell2(String string, int alignment, Font canFont) {
		PdfPCell cell = new PdfPCell(new Phrase(string, canFont));
		cell.setPadding(3);
		cell.setHorizontalAlignment(alignment);
		return cell;
	}

	public PdfPCell getCellSpan(String string, int alignment, Font canFont, int span) {
		PdfPCell cell = new PdfPCell(new Phrase(string, canFont));
		cell.setPadding(3);
		cell.setRowspan(span);
		cell.setHorizontalAlignment(alignment);
		cell.setBorder(PdfPCell.NO_BORDER);
		return cell;
	}

	public PdfPCell getCellColSpan(String string, int alignment, Font canFont, int span) {
		PdfPCell cell = new PdfPCell(new Phrase(string, canFont));
		cell.setPadding(6);
		cell.setColspan(span);
		cell.setHorizontalAlignment(alignment);
		cell.setBorder(PdfPCell.NO_BORDER);
		return cell;
	}

}
