import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {

    let x = this.test.split('|')

    x.forEach(data => {
      if (data.length != 0) {
        let a = {}
        let z = data.split(":");
        if (z.length != 0) {
          let month = z[0];
          let y = z[1].split(',')
          let year = y[0];
          let b = y[1];
          a = {
            month: month,
            year: year,
            data: b
          }
          this.list.push(a)
        }
      }
    })
    console.log(this.list)

  }
  test: string = "Dec:2017,000/XXX|Nov:2017,DDD/DDD|Oct:2017,000/XXX|Sep:2017,000/XXX|Aug:2017,000/XXX|Jul:2017,000/XXX|Jun:2017,000/XXX|May:2017,000/XXX|Apr:2017,000/XXX|Mar:2017,000/XXX|Feb:2017,000/XXX|Jan:2017,000/XXX|Dec:2016,000/XXX|Nov:2016,000/XXX|Oct:2016,000/XXX|Sep:2016,000/XXX|Aug:2016,000/XXX|Jul:2016,000/XXX|Jun:2016,000/XXX|May:2016,000/XXX|Apr:2016,000/XXX|Mar:2016,000/XXX|Feb:2016,000/XXX|Jan:2016,000/XXX|Dec:2015,000/XXX|Nov:2015,000/STD|Oct:2015,000/STD|Sep:2015,000/STD|Aug:2015,000/STD|Jul:2015,000/STD|Jun:2015,000/STD|May:2015,DDD/DDD|Apr:2015,DDD/DDD|Mar:2015,000/STD|Feb:2015,DDD/DDD|Jan:2015,XXX/XXX|";
  list: any = [];
}
