package com.bhushan.constructor;

public class ConstructorExample {

	//InstanceVariable
	String emp_name;
	int emp_id;
	
	
	public ConstructorExample(String emp_name,int emp_id) {
		this.emp_id=emp_id;
		this.emp_name=emp_name;
		
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		ConstructorExample constructorExample = new ConstructorExample("Nitesh", 101);
		ConstructorExample constructorExample2 = new ConstructorExample("Ragini", 102);
	}
	
}
