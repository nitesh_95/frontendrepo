package com.bhushan;

public class ArrayConceptCase1 {
	
	public static void main(String[] args) {
		int age[] = {10,20,30};
		
		for (int i : age) {
			System.out.println(i);
		}
	}

}
