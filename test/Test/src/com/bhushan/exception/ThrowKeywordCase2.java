package com.bhushan.exception;

public class ThrowKeywordCase2 {

	public void divideByZero() {
		throw new ArithmeticException("This is not possible");
	}

	public static void main(String[] args) {
		ThrowKeywordCase2 throwKeywordCase2 = new ThrowKeywordCase2();
		throwKeywordCase2.divideByZero();

	}

}
