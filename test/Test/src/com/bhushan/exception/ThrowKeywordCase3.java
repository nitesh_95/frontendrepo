package com.bhushan.exception;

import java.io.FileNotFoundException;

public class ThrowKeywordCase3 {

	public void getFile() throws FileNotFoundException {
		throw new FileNotFoundException("File not found");
	}

	public static void main(String[] args) {
		try {
			ThrowKeywordCase3 throwKeywordCase3 = new ThrowKeywordCase3();
			throwKeywordCase3.getFile();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

}
