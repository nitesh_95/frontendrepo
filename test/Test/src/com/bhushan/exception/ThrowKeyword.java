package com.bhushan.exception;

public class ThrowKeyword {
	
	public void validate(int age ) {
		if(age<21) {
		throw new ArithmeticException("You are not eligible to vote");	
		}else {
			System.out.println("Welcome to Vote");
		}
	}

	public static void main(String[] args) {
		ThrowKeyword throwKeyword = new ThrowKeyword();
		throwKeyword.validate(18);
		
	}

}
