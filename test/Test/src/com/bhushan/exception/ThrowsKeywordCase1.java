package com.bhushan.exception;

import java.io.IOException;

public class ThrowsKeywordCase1 {

	public void getDeviceData() throws IOException  {
		throw new IOException("Device Error");
	}

	public static void main(String[] args) throws IOException {
		ThrowsKeywordCase1 throwsKeywordCase1 = new ThrowsKeywordCase1();
		throwsKeywordCase1.getDeviceData();
	}
}
