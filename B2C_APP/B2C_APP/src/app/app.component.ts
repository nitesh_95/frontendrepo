import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { UserIdleService } from 'angular-user-idle';
import { Subscription } from 'rxjs';
import { JsonPipe } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title = 'B2CAPP';
  showHead = true
  showslider = true
  showfooter = true;
  public common_ip: any = 'http://202.65.144.147:8855';
  constructor(private router: Router, private userIdle: UserIdleService) {

  }
  ngOnInit() {
    sessionStorage.setItem('commonIP', JSON.stringify(this.common_ip))
    var x = JSON.parse(sessionStorage.getItem('commonIP'))

  }
}