import { Injectable, OnInit } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ViewApplicationService implements OnInit {
 
  public productType:any = []
  public productTy:any = []
  public checkList:any= []
  public updateProduct:any = []
  constructor() { }
  ngOnInit() { }

 

  productTypes(productType: any) {
    this.productType = productType;
    sessionStorage.setItem('productType', JSON.stringify(this.productType))
  }
    productTyp(productTy:any){
      this.productTy = productTy;
      sessionStorage.setItem('productTy', JSON.stringify(this.productTy))
  }
  
  checkLists(checkList: any) {
    this.checkList = checkList;
    sessionStorage.setItem('checkList', JSON.stringify(this.checkList))
    
  }
  updateProducts(updateProduct: any) {
    this.updateProduct = updateProduct;
    sessionStorage.setItem('updateProduct', JSON.stringify(this.updateProduct))
    
  }
}

